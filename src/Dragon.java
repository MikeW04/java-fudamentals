public class Dragon extends Pet{

        public Dragon(){
            this.setNumLegs(4);
        }

        public Dragon(String name){
            this();
            this.setName(name);
        }

        // method overloading
        public void feed(){
            System.out.println("Feed pet sheep");
        }

}
