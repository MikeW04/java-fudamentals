public class Pet {

    private String name;
    private int numLegs;

    public Pet(){
        numPets++;
    }

    // Static =
    public static int numPets = 0;

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void feed(){
        System.out.println("pet fed");
    }



}
