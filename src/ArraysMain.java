import java.util.ArrayList;

public class ArraysMain {

    // shortcut tye main + enter
    public static void main(String[] args) {

        //java collections

        // this arraylist will only contain Pets
        ArrayList<Pet> myPetList = new ArrayList<Pet>();


        int[] intArray = {2,5,76,44,22};

        // declare an array
        Pet[] petArray;

        //initialise the array
        petArray = new Pet[10];

        //


        System.out.println();

        //put a pet in the array
        Pet firstPet = new Pet();
        firstPet.setName("First Pet");

        petArray[0] =firstPet;

        //put a dragon in the array
        Dragon myDragon = new Dragon();
        myDragon.setName("Spyro");

        petArray[1] = myDragon;

        for(int i=0; i < petArray.length; i++){
            System.out.println(petArray[i]);
        }

        petArray[0].feed();
        petArray[1].feed();


    }
}
