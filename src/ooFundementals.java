public class ooFundementals {

    public static void main(String[] args) {

        Pet myPet= new Pet();
        Dragon myDragon = new Dragon();

        System.out.println("Before anything" + myPet.numPets);




        myPet.setNumLegs(4);
        myPet.setName("Hugo");


        myDragon.setName("Dragon");



        System.out.println("My pet is called: " +myPet.getName());
        System.out.println("My pet has "+myPet.getNumLegs()+" legs");
        myPet.feed();

        System.out.println();

        System.out.println("My dragon is called: " +myDragon.getName());
        System.out.println("My dragon has "+myDragon.getNumLegs()+" legs");

        myDragon.feed();
    }
}
