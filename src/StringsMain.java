public class StringsMain {


    public static void main(String[] args) {

        String bigString = "start: ";

        StringBuilder stringBuilder = new StringBuilder();


        for(int i =0 ; i<10 ; ++i){
            stringBuilder.append(i);
        }

        System.out.println(stringBuilder);

        String testString = "hello" + " " + "world" + " " + "after" + " " + "lunch";

        System.out.println(testString);

        Dragon stringyDragon = new Dragon("Smaug");

        System.out.println("Dragon created:" + stringyDragon.toString());


    }
}
